from campaigns import models
from campaigns import serializers
from rest_framework import viewsets, permissions


class ProjectViewSet(viewsets.ModelViewSet):
    """ViewSet for the Project class"""

    queryset = models.Project.objects.all()
    serializer_class = serializers.ProjectSerializer
    permission_classes = [permissions.IsAuthenticated]


class FundingViewSet(viewsets.ModelViewSet):
    """ViewSet for the Funding class"""

    queryset = models.Funding.objects.all()
    serializer_class = serializers.FundingSerializer
    permission_classes = [permissions.IsAuthenticated]


