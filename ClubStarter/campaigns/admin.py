from django.contrib import admin
from django import forms
from .models import Project, Funding

class ProjectAdminForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = '__all__'


class ProjectAdmin(admin.ModelAdmin):
    form = ProjectAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'target', 'overFundingAllowed', 'description', 'targetdate']
    # readonly_fields = ['name', 'slug', 'created', 'last_updated', 'target', 'overFundingAllowed', 'description', 'targetdate']

admin.site.register(Project, ProjectAdmin)


class FundingAdminForm(forms.ModelForm):

    class Meta:
        model = Funding
        fields = '__all__'


class FundingAdmin(admin.ModelAdmin):
    form = FundingAdminForm
    list_display = ['created', 'last_updated', 'comment']
    readonly_fields = ['created', 'last_updated', 'comment']

admin.site.register(Funding, FundingAdmin)


