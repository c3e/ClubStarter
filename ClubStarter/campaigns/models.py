from django.urls import reverse
from django.core.mail import send_mail
from django.db.models import *
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth import models as auth_models
from django.db import models as models
from django.utils import timezone
from django.utils.functional import cached_property

from . import default_settings
from django.conf import settings
if not settings.configured:
    settings.configure(myapp_defaults)

class Project(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    created = models.DateTimeField("Erstelldatum", auto_now_add=True, editable=False)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    last_updated = models.DateTimeField("Letztes Update", auto_now=True, editable=False)
    target = models.DecimalField("Ziel in Euro", max_digits=10, decimal_places=2)
    overFundingAllowed = models.BooleanField("Überfinanzierbar")
    description = models.TextField("Beschreibung")
    targetdate = models.DateTimeField("Zieldatum")

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.name

    def get_absolute_url(self):
        return reverse('campaigns_project_detail', args=(self.slug,))

    def get_update_url(self):
        return reverse('campaigns_project_update', args=(self.slug,))

    def get_mail_url(self):
        return reverse('campaigns_project_mail', args=(self.slug,))

    def get_fund_url(self):
        return reverse('campaigns_project_fund', args=(self.slug,))

    @cached_property
    def funded_sum(self):
        return self.funding_set.aggregate(models.Sum('amount'))['amount__sum'] or 0

    @cached_property
    def is_fundable(self):
        return ((self.overFundingAllowed or (self.target > self.funded_sum)) and (timezone.now() < self.targetdate))

    def mail_funders(self, subject, msg):
        """send an email to all funders of the project"""
        mail_to = self.funding_set.values_list('funder__email', flat=True)
        send_mail('[CLUBSTARTER]' + subject, msg, settings.MAIL_FROM, mail_to)


class Funding(models.Model):

    # Fields
    created = models.DateTimeField("Erstelldatum", auto_now_add=True, editable=False)
    last_updated = models.DateTimeField("Letztes Update", auto_now=True, editable=False)
    comment = models.TextField("Kommentar", max_length=100, blank=True)
    amount = models.PositiveIntegerField("Betrag", default=0)
    payed = models.BooleanField("Bezahlt", default=False)

    # Relationship Fields
    funder = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    Project = models.ForeignKey('campaigns.project', on_delete=models.CASCADE)

    class Meta:
        ordering = ('-created',)
        unique_together = (("funder", "Project"),)

    def __str__(self):
        return u'Funding %s' % self.pk

    def get_absolute_url(self):
        return reverse('campaigns_funding_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('campaigns_funding_update', args=(self.pk,))
