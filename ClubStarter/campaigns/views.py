from django.views.generic import DetailView, ListView, UpdateView, CreateView
from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.http import HttpResponseForbidden
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Project, Funding
from .forms import ProjectForm, FundingForm, MailForm


class ProjectListView(ListView):
    model = Project


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    form_class = ProjectForm

    def form_valid(self, form):
        form.instance.creator = self.request.user
        return super(ProjectCreateView, self).form_valid(form)

class ProjectDetailView(DetailView):
    model = Project


class ProjectUpdateView(LoginRequiredMixin, UpdateView):
    model = Project
    form_class = ProjectForm

    def get_object(self, *args, **kwargs):
        obj = super(ProjectUpdateView, self).get_object(*args, **kwargs)
        if (obj.creator != self.request.user):
            raise PermissionDenied()
        return obj

class FundingListView(ListView):
    model = Funding


@login_required
def fundProject(request, slug):
    project = get_object_or_404(Project, slug=slug)
    if project.is_fundable:
        if request.method == 'POST':
            form = FundingForm(request.POST)
            if form.is_valid():
                Funding = form.save(commit=False)
                Funding.funder = request.user
                Funding.Project = project
                Funding.save()
                return redirect(project.get_absolute_url())
        else:
            form = FundingForm()

        return render(request, 'campaigns/project_fund.html', {'form': form})
    return HttpResponseForbidden()

@login_required
def mailProject(request, slug):
    project = get_object_or_404(Project, slug=slug)
    if (project.creator == request.user) or request.user.is_superuser:
        if request.method == 'POST':
            form = MailForm(request.POST)
            if form.is_valid():
                try:
                    project.mail_funders(form.cleaned_data['subject'], form.cleaned_data['message'])
                    messages.success(request, 'Mail verschickt')
                except Exception as e:
                    messages.warning(request, str(e))
                redirect(project.get_absolute_url())
        else:
            form = MailForm()
        return render(request, 'campaigns/mail_form.html', {'object': project, 'form': form})

    return HttpResponseForbidden()


class FundingDetailView(LoginRequiredMixin, DetailView):
    model = Funding


class FundingUpdateView(LoginRequiredMixin, UpdateView):
    model = Funding
    form_class = FundingForm

    def get_success_url(self):
        # we don't care for a detailed view for this model.
        # get the related project url instead

        return self.object.Project.get_absolute_url()

    def get_object(self, *args, **kwargs):
        obj = super(FundingUpdateView, self).get_object(*args, **kwargs)
        if (obj.funder != self.request.user):
            raise PermissionDenied()
        return obj
