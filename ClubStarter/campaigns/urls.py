from django.conf.urls import url, include
from django.conf import settings
from rest_framework import routers
from campaigns import api
from campaigns import views

if settings.INCLUDE_CLUBSTARTER:
    router = routers.DefaultRouter()
    router.register(r'project', api.ProjectViewSet)
    router.register(r'funding', api.FundingViewSet)


    urlpatterns = (
        # urls for Django Rest Framework API
        url(r'^api/', include(router.urls)),
    )

    urlpatterns += (
        # urls for Project
        url(r'^project/$', views.ProjectListView.as_view(), name='campaigns_project_list'),
        url(r'^project/create/$', views.ProjectCreateView.as_view(), name='campaigns_project_create'),
        url(r'^project/detail/(?P<slug>\S+)/$', views.ProjectDetailView.as_view(), name='campaigns_project_detail'),
        url(r'^project/update/(?P<slug>\S+)/$', views.ProjectUpdateView.as_view(), name='campaigns_project_update'),
    )

    urlpatterns += (
        # urls for Funding
        url(r'^project/fund/(?P<slug>\S+)/$', views.fundProject, name='campaigns_project_fund'),
        url(r'^project/mail/(?P<slug>\S+)/$', views.mailProject, name='campaigns_project_mail'),
        url(r'^funding/$', views.FundingListView.as_view(), name='campaigns_funding_list'),
        url(r'^funding/detail/(?P<pk>\S+)/$', views.FundingDetailView.as_view(), name='campaigns_funding_detail'),
        url(r'^funding/update/(?P<pk>\S+)/$', views.FundingUpdateView.as_view(), name='campaigns_funding_update'),
    )

else:
    urlpatterns = ()