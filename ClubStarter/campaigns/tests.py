import unittest
from random import randint

from django.urls import reverse
from django.test import Client
from .models import Project, Funding
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone


def create_django_contrib_auth_models_user(**kwargs):
    rnd = str(randint(1, 1000))
    defaults = {}
    defaults["username"] = "username" + rnd
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_Project(**kwargs):
    rnd = str(randint(1,1000))
    defaults = {}
    defaults["name"] = "name" + rnd
    defaults["slug"] = "name" + rnd
    defaults["creator_id"] = 1
    defaults["target"] = 9001
    defaults["overFundingAllowed"] = False
    defaults["description"] = "description"
    defaults["targetdate"] = timezone.now()
    defaults.update(**kwargs)
    return Project.objects.create(**defaults)


def create_Funding(**kwargs):
    defaults = {}
    defaults["comment"] = "comment"
    defaults.update(**kwargs)
    if "funder" not in defaults:
        defaults["funder"] = create_django_contrib_auth_models_user()
    if "Project" not in defaults:
        defaults["Project"] = create_Project()
    return Funding.objects.create(**defaults)


class ProjectViewTest(unittest.TestCase):
    '''
    Tests for Project
    '''
    def setUp(self):
        self.client = Client()

    def test_list_Project(self):
        url = reverse('campaigns_project_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_Project(self):
        url = reverse('campaigns_project_create')
        data = {
            "name": "name2",
            "slug": "name2",
            "target": "target",
            "overFundingAllowed": "overfundingAllowed",
            "description": "description",
            "targetdate": "targetdate",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_Project(self):
        Project = create_Project()
        url = reverse('campaigns_project_detail', args=[Project.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_Project(self):
        Project = create_Project()
        data = {
            "name": "name3",
            "slug": "name3",
            "target": "target",
            "overFundingAllowed": "overfundingAllowed",
            "description": "description",
            "targetdate": "targetdate",
        }
        url = reverse('campaigns_project_update', args=[Project.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class FundingViewTest(unittest.TestCase):
    '''
    Tests for Funding
    '''
    def setUp(self):
        self.client = Client()

    def test_list_Funding(self):
        url = reverse('campaigns_funding_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_detail_Funding(self):
        Funding = create_Funding()
        url = reverse('campaigns_funding_detail', args=[Funding.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_update_Funding(self):
        Funding = create_Funding()
        data = {
            "comment": "comment",
            "funder": create_django_contrib_auth_models_user().pk,
            "Project": create_Project().pk,
        }
        url = reverse('campaigns_funding_update', args=[Funding.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


