from campaigns import models

from rest_framework import serializers


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Project
        fields = (
            'slug',
            'name',
            'created',
            'last_updated',
            'target',
            'overFundingAllowed',
            'description',
            'targetdate',
            'funded_sum',
            'is_fundable'
        )


class FundingSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Funding
        fields = (
            'pk',
            'created',
            'last_updated',
            'comment',
        )


