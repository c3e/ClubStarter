from django import forms
from .models import Project, Funding


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ['name', 'slug', 'target',  'targetdate', 'overFundingAllowed', 'description']


class FundingForm(forms.ModelForm):
    class Meta:
        model = Funding
        fields = ['amount', 'payed', 'comment']


class MailForm(forms.Form):
    subject = forms.CharField(label='Betreff', max_length=100)
    message = forms.CharField(widget=forms.Textarea)
