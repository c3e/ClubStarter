from django import template
from django.contrib.auth.models import AnonymousUser, User
from django.db.models import Count, Avg
from django.utils import timezone
from datetime import datetime, timedelta
from campaigns.models import Project
from django.conf import settings
from django.utils import timezone
register = template.Library()

@register.simple_tag()
def active_campaign_cnt():
    now = timezone.now()
    return Project.objects.filter(targetdate__gt=now).count()

@register.simple_tag()
def clubstarter_enabled():
    return settings.INCLUDE_CLUBSTARTER